#!/usr/bin/env python3
import sys
from random import randint
import operator

class Statistics(object):
    # Result Statistics

    def __init__(self):
        # Total count
        self.correct = 0
        self.error   = 0
        self.helpers = 0

        # Current iteration
        self.c_corr  = 0
        self.c_err   = 0

    def reset_current(self):
        # Reset the last ten counters
        self.c_corr = 0
        self.c_err = 0

    def last_ten(self):
        # Returning True if the user has completed a set of ten exercises. Otherwise the method returns False
        return (self.c_corr + self.c_err) % 10 == 0
    
    def latest_results(self):
        s = "Du hade " + str(self.c_corr) + " rätt och " + str(self.c_err) + " fel."
        self.reset_current()
        if self.c_err == 0: 
            s += "GRATTIS!\n"
        s += "Total andel rätta svar: " + str(round(100 * self.correct / (self.correct + self.error))) + "%\n"
        s += "Totala antalet ledtrådar: " + str(self.error)
        return s

#########################
# create next problem   #
#########################

def get_next(easy=False):

    def mul_p(easy):
        if easy:
            x = randint(1, 7)
            y = randint(1, 10)
        else:
            x = randint(2, 12)
            y = randint(2, 12)

        return (operator.mul, " * ", x, y)

    def div_p(easy):
        x = randint(1, 5)
        y = randint(0, 10)

        return (operator.truediv, " / ", operator.mul(x,y), y)
    funcs = {
        0: mul_p,
        1: div_p,
        }

    cat = randint(0, len(funcs))
    
    return funcs[cat](easy)


#######################
# analyse middle step #
#######################

def create_clue(problem):

    func = problem[0]
    token = problem[1]
    x = problem[2]
    y = problem[3]
    
    # Multiplication
    if func == operator.mul:
        ret_list = [] # Returned list
        inc = 0
        while inc < y:
            ret_list.append(str(x))
            inc+=1
        clue = str(x) + token + str(y) + ' = ' + ' + '.join(ret_list) + ' = '

    # Division
    elif func == operator.truediv:
        # TODO: we need some instructive way of giving helpful hints.
        # Maybe something like: "What number X multipled with Y becomes Z?"
        clue = "Tyvärr, ingen ledtråd att tillgå"
    elif func == operator.add:
        # TODO: Add addition hints
        clue = "Tyvärr, ingen ledtråd att tillgå"
    else:
        # TODO: Add hints for subtraction
        clue = "Tyvärr, ingen ledtråd att tillgå"
    return clue

#########################
# do the actual testing #
#########################

def test(results, problem):
    
    def get_number(question):
        # check input - keep going until input is valid
        a=-1
        while a < 0:
            try:
                a = int(input(question))
                return a
            except ValueError:
                print("Anvand bara siffror, avsluta med RETURN")
                a = -1

    def correct_answer(answer, results):
        # Check answer & update results        
        if answer == func(x, y):
            print ("Bra")
            results.correct += 1
            results.c_corr += 1
            return True
        else:
            return False

    func = problem[0]
    token = problem[1]
    x = problem[2]
    y = problem[3]
    
    problem_str = str(x) + token + str(y) + ' = '

    answer = get_number(problem_str) 

    if not correct_answer(answer, results):       
        # Provide a hint and give a second chance 
        results.hrlpers += 1
        print("Fel, skapar en ledtråd!")
        answer = get_number(create_clue(problem))

        if not correct_answer(answer, results):
            # blew it again, report error
            print("Fel, svaret är ", func(x, y))
            results.error += 1
            results.c_err += 1
                
########
# Main #
########

def main():
    easy = False
    if "easy" in sys.argv[1:]:
        easy = True
    
    results = Statistics()
    
    while(True):
        try:
            problem = get_next(easy)
            test(results, problem)
            if results.last_ten():
                print(results.latest_results())
        except KeyboardInterrupt:
            print(results.latest_results())
            print ("\nTack och hej, Fabifjonkpastej!")
            sys.exit()
main()
