#!/usr/bin/env python3

import sys

def show_help():
    print("Usage: %s [add|save] <key>" % sys.argv[0])
    print("%s add" % sys.argv[0])
    print("to get the first hint, plus key for next hint")
    sys.exit()


if len(sys.argv) == 1:
    show_help()

if sys.argv[1] == "add":
    fname = ".add-hints"
elif sys.argv[1] == "save":
    fname = ".save-hints"
else:
    show_help()

if len(sys.argv) == 3:
    key = sys.argv[2]
else:
    key = None

def decode(k):
    s = ""
    for i in range(0, len(k), 2):
        s += chr(int(bytes(k[i:i+2], 'utf-8'), 16))
    return s


hints = open(fname).read().split("\n")

for i in range(0, len(hints), 2):
    this_key = hints[i]
    if key is None or key == this_key:
        print("\nHint:\n")
        s = decode(hints[i+1])
        print((len(s)+4)*'-' + '\n',
              s, '\n' +
              (len(s)+4)*'-' + '\n')

        if not hints[i+2] == '0000':
            print("To get next hint run:\n ./%s %s %s" %
                  (sys.argv[0], sys.argv[1], hints[i+2]))
        else:
            print("No more hints! This is the very last one")
        break
else:
    print("No such key!")
    





